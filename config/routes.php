<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope(
    '/articles',
    ['controller' => 'Articles'],
    function ($routes) {
        $routes->connect('/tagged/*', ['action' => 'tags']);
    }
);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    //$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->connect('logs', ['controller' => 'logs', 'action' => 'index']);
    $routes->connect('locale', ['controller' => 'Localizations', 'action' => 'index']);
    $routes->connect('email', ['controller' => 'Emails', 'action' => 'index']);
    $routes->connect('sessionobject', ['controller' => 'Sessions', 'action' => 'index']);
    $routes->connect('sessionread', ['controller' => 'Sessions', 'action' => 'retrieve_session_data']);
    $routes->connect('sessionwrite', ['controller' => 'Sessions', 'action' => 'write_session_data']);
    $routes->connect('sessioncheck', ['controller' => 'Sessions', 'action' => 'check_session_data']);
    $routes->connect('sessiondelete', ['controller' => 'Sessions', 'action' => 'delete_session_data']);
    $routes->connect('sessiondestroy', ['controller' => 'Sessions', 'action' => 'destroy_session_data']);
    $routes->connect('cookie/write', ['controller' => 'Cookies', 'action' => 'write_cookie']);
    $routes->connect('cookie/read', ['controller' => 'Cookies', 'action' => 'read_cookie']);
    $routes->connect('cookie/check', ['controller' => 'Cookies', 'action' => 'check_cookie']);
    $routes->connect('cookie/delete', ['controller' => 'Cookies', 'action' => 'delete_cookie']);
    $routes->connect('login', ['controller' => 'Logins', 'action' => 'index']);
    $routes->connect('validation', ['controller' => 'Valids', 'action' => 'index']);
    $routes->connect('users', ['controller' => 'Users', 'action' => 'index']);
    $routes->connect('/', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('forgot-password', ['controller' => 'Users', 'action' => 'password']);
    //$routes->connect('other', ['controller' => 'Valids', 'action' => 'other']);
    $routes->fallbacks(DashedRoute::class);
});
