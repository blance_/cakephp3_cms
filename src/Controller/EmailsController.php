<?php 

namespace App\Controller;
use App\Controller\AppController;
use Cake\Mailer\Email;

class EmailsController extends AppController {

    
    public function index() {
        $email = new Email();
        $email  -> transport('gmail')
                -> to('blancessanchez30@gmail.com')
                -> from(['testemail.yema@gmail.com' => 'Hello'])
                -> subject('Test Email')
                -> send('My message');
    }
}