
<?php echo $this->Form->control('victim', ['type' => 'radio', 'label' => 'Victim is: ', 'options' => ['Business', 'Individual'], 'required' => true]); ?>

<div class="business" style="display:none">
    <?php
        echo $this->Form->control('business_name', ['required' => true]);
        echo $this->Form->control('business_telephone');
        echo $this->Form->control('business_address', ['required' => true]);
        echo $this->Form->control('business_city', ['required' => true]);
        echo $this->Form->control('business_state', ['required' => true, 'options' => $states]);
        echo $this->Form->control('business_zip', ['required' => true]);
    ?>
</div>

<div class="individual" style="display:none">
    <?php 
        echo $this->Form->control('name', ['required' => true]);
        echo $this->Form->control('telephone');
        echo $this->Form->control('address', ['required' => true]);
        echo $this->Form->control('city', ['required' => true]);
        echo $this->Form->control('state', ['required' => true, 'options' => $states]);
        echo $this->Form->control('zip', ['required' => true]);
    ?>
</div>

<script>
    if (jQuery('#victim-0').is(':checked')) {
        showAndRequire('.business', true);
        showAndRequire('.individual', false);
    } else if (jQuery('#victim-1').is(':checked')) {
        showAndRequire('.individual', true);
        showAndRequire('.business', false);
    }

    jQuery('#victim-0').click(function() {
        showAndRequire('.business', true);
        showAndRequire('.individual', false);
    });
    jQuery('#victim-1').click(function() {
        showAndRequire('.individual', true);
        showAndRequire('.business', false);
    });

    function showAndRequire(section, onOff) {
        jQuery(section).toggle(onOff);
        if (onOff) {
            jQuery(section + ' .required input').attr('required', onOff);
            jQuery(section + ' .required select').attr('required', onOff);
        } else {
            jQuery(section + ' .required input  ').removeAttr('required');
            jQuery(section + ' .required select').removeAttr('required');
        }
    }
</script>