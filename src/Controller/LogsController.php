<?php

namespace App\Controller;
use Cake\Log\Log;

class LogsController extends AppController {
    
    public function index() {
        Log::write('debug', 'Something didn\'t work.');

        $this->log('Something didn\'t work.', 'debug');
    }
}
