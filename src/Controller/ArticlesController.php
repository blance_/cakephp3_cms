<?php

namespace App\Controller;

use App\Controller\AppController;

class ArticlesController extends AppController {

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Articles.title' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->Auth->allow(['tags']);
    }
    
    public function index() {
        $articles = $this->paginate($this->Articles);
        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }

    public function view($slug = null) {
        $article = $this->Articles->findBySlug($slug)->firstOrFail();
        $this->set(compact('article'));
    }

    public function add() {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            $article->user_id = $this->Auth->user('id');

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your articles has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your article'));
        }
        $this->set('article', $article);
    }

    public function edit($slug) {
        //$user = $this->Users->get($id);
        $article = $this->Articles
            ->findBySlug($slug)
            ->contain('Tags')
            ->firstOrFail();
        if ($this->request->is(['post', 'put'])) {
            $this->Articles->patchEntity($article, $this->request->getData(), [
                'accessibleFields' => ['user_id' => false]
            ]);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been updated'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your article'));
        }
        $this->set('article', $article);
    }

    public function delete($slug) {
        $user = $this->Users->get($id);
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->findBySlug($slug)->firstOrFail();
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The {0} article has been deleted.', $article->title));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function tags(...$tags) {
        //$tags = $this->request->getParam('pass');
        $articles = $this->Articles->find('tagged', ['tags' => $tags]);
        $this->set(['articles' => $articles, 'tags'=>$tags]);
    }

    public function isAuthorized($user) {
        $action = $this->request->getPasram('action');
        if (in_array($action, ['add', 'tags'])) {
            return true;
        }
        $slug = $this->request->getParam('pass.0');
        if (!$slug) {
            return false;
        }
        $article = $this->Article->findBySlug($slug)->first();
        return $article->user_id == $user['id'];
    }
}