<?php 

namespace App\Controller;
use App\Controller\AppController;

class SessionsController extends AppController {
    public function retrieveSessionData() {
        $session = $this->request->getSession();
        $email = $session->read('Auth.User.email');
        $this->set('email', $email);
    }

    public function writeSessionData() {
        $session = $this->request->getSession();
        $session->write('name', 'blancessanchez30@gmail.com');
    }

    public function checkSessionData() {
        $session = $this->request->getSession();
        $email = $session->check('Auth.User.email');
        $this->set('email', $email);
    }

    public function deleteSessionData() {
        $session = $this->request->getSession();
        $session->delete('name');
    }

    public function destroySessionData() {
        $session = $this->request->session();
        $session->destroy();
    }
}