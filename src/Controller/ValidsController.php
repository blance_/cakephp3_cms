<?php

    namespace App\Controller;
    use App\Controller\AppController;
    use Cake\Validation\Validator;
    use Cake\Http\Client;

    class ValidsController extends AppController {
        
        public function index() {
            $validator = new Validator();
            $validator ->notEmpty('username', 'Please fill up username. <br>');
            $validator ->notEmpty('password', 'Please fill up password. <br>');
            $validator ->notEmpty('confirm_password', 'Please fill up confirm password. <br>');
            $validator ->notEmpty('email', 'Please fill up email address. <br>');
            $errors = $validator->errors($this->request->getData());
            $this->set('errors', $errors);

            $data = [
                'username' => $this->request->getData('username'),
                'email' => $this->request->getData('email'),
            ];
            echo json_encode($data);
        }

        public function testJson() {
            $id = 3;
            $http = new Client();
            $response = $http->get('http://virtserver.swaggerhub.com/chinsoft-ando/reservation/1.0.0/shops/'.$id);
            $json = $response->json;
            pr($json);
            die();
        }

        public function other() {

        }
    }