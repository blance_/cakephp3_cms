<h1> Articles </h1>
<?= $this->Html->link('Add Article', ['action' => 'add']); ?>
<table>
    <tr>
        <th><?= $this->Paginator->sort('id') ?></th>
        <th><?= $this->Paginator->sort('title') ?></th>
        <th><?= $this->Paginator->sort('created') ?></th>
        <th><?= __('Actions') ?></th>
    </tr>

    <?php foreach ($articles as $article) : ?>
        <tr>
            <td><?= $this->Number->format($article->id) ?></td>
            <td><?= $this->Html->link(h($article->title), ['action' => 'view', $article->slug]) ?></td>
            <td><?= h($article->created->format(DATE_RFC850)) ?></td>
            <td>
                <?= $this->Html->link('Edit', ['action' => 'edit', $article->slug]) ?>
                <?= $this->Form->postLink('Delete',
                ['action' => 'delete', $article->slug],
                ['confirm' => 'Are you sure?'])
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>