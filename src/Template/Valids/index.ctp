<!-- <div id="app">
    {{ status }}
</div> -->
<?php 
    if ($errors) {
        foreach ($errors as $error)
            foreach ($error as $msg)
                echo '<font color="red">' . $msg . '</font>';
    } else {
        echo "No errors";
    }

    echo $this->Form->create('Logins', ['id' => 'myForm']);
    echo $this->Form->control('username', ['id' => 'username']);
    echo $this->Form->control('password');
    echo $this->Form->control('confirm_password', ['type' => 'password']);
    echo $this->Form->control('email', ['id' => 'email']);
    echo $this->Form->button(__('Submit'));
    echo $this->Form->end();
?>

<div id="result"></div>

